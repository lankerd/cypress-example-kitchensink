context('Actions', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/actions')
  })

  // **** Actions ****
  //
  // Let's perform some actions on DOM elements
  // Move of these involve filling in form elements
  // But some actions, like click, will often be
  // used throughout an application

  it('cy.type() - type into a DOM element', function(){

    // https://on.cypress.io/api/type
    cy
      .get('.action-email')
      .type('fake@email.com').should('have.value', 'fake@email.com')

      // cy.type() may include special character sequences
      .type('{leftarrow}{rightarrow}{uparrow}{downarrow}{del}{selectall}{backspace}')

      // **** Type Options ****
      //
      // cy.type() accepts options that control typing
      //
      // Delay each keypress by 0.1 sec
      // You may want to set the delay which
      // causes the keystrokes to happen much slower
      // in some situations if the application under
      // test is not able to handle rapid firing events.
      // (generally due to the app not properly throttling events)
      .type('slow.typing@email.com', {delay: 100}).should('have.value', 'slow.typing@email.com')

      .get('.action-disabled')

      // Ignore error checking prior to type
      // like whether the input is visible or disabled
      .type('disabled error checking', {force: true})
        .should('have.value', 'disabled error checking')

  })

  it('cy.focus() - focus on a DOM element', function(){

    // https://on.cypress.io/api/focus
    cy
      .get('.action-focus').focus()
      .should('have.class', 'focus')
        .prev().should('have.attr', 'style', 'color: orange;')

  })

  it('cy.blur() - blur off a DOM element', function(){

    // https://on.cypress.io/api/blur
    cy
      .get('.action-blur').type('I\'m about to blur').blur()
      .should('have.class', 'error')
        .prev().should('have.attr', 'style', 'color: red;')

  })


  it('cy.clear() - clears the value of an input or textarea element', function(){

    // https://on.cypress.io/api/clear
    cy
      .get('.action-clear').type('We are going to clear this text')
        .should('have.value', 'We are going to clear this text')
      .clear()
        .should('have.value', '')

  })

  it('cy.submit() - submit a form', function(){

    // https://on.cypress.io/api/submit
    cy
      .get('.action-form')
        .find('[type="text"]').type('HALFOFF')
      .get('.action-form').submit()
        .next().should('contain', 'Your form has been submitted!')

  })

  it('cy.click() - click on a DOM element', function(){

    // https://on.cypress.io/api/click
    cy.get('.action-btn').click()

    // **** Click Position ****
    //
    // cy.click() accepts a position argument
    // that controls where the click occurs
    //
    // clicking in the center of the element is the default
    cy.get('#action-canvas').click()

    // click the top left corner of the element
    cy.get('#action-canvas').click('topLeft')

    // click the top right corner of the element
    cy.get('#action-canvas').click('topRight')

    // click the bottom left corner of the element
    cy.get('#action-canvas').click('bottomLeft')

    // click the bottom right corner of the element
    cy.get('#action-canvas').click('bottomRight')

    // **** Click Coordinate ****
    //
    // cy.click() accepts a an x and y coordinate
    // that controls where the click occurs :)

    cy
      .get('#action-canvas')
        // click 80px on x coord and 75px on y coord
        .click(80, 75)
        .click(170, 75)
        .click(80, 165)
        .click(100, 185)
        .click(125, 190)
        .click(150, 185)
        .click(170, 165)

    // **** Click Options ****
    //
    // cy.click() accepts options that control clicking
    //
    // click multiple elements by passing multiple: true
    // otherwise an error will be thrown if multiple
    // elements are the subject of cy.click
    cy.get('.action-labels>.label').click({multiple: true})

    // Ignore error checking prior to clicking
    // like whether the element is visible, clickable or disabled
    // this button below is covered by another element.
    cy.get('.action-opacity>.btn').click({force: true})

  })

  it('cy.dblclick() - double click on a DOM element', function(){

    // We have a listener on 'dblclick' event in our 'scripts.js'
    // that hides the div and shows an input on double click

    // https://on.cypress.io/api/dblclick
    cy
      .get('.action-div').dblclick().should('not.be.visible')
      .get('.action-input-hidden').should('be.visible')

  })

  it('cy.check() - check a checkbox or radio element', function(){

    // By default, cy.check() will check all
    // matching checkbox or radio elements in succession, one after another

    // https://on.cypress.io/api/check
    cy
      .get('.action-checkboxes [type="checkbox"]').not('[disabled]').check().should('be.checked')

      .get('.action-radios [type="radio"]').not('[disabled]').check().should('be.checked')

    // **** Check Value ****
    //
    // cy.check() accepts a value argument
    // that checks only checkboxes or radios
    // with matching values
    //
      .get('.action-radios [type="radio"]').check('radio1').should('be.checked')

    // **** Check Values ****
    //
    // cy.check() accepts an array of values
    // that checks only checkboxes or radios
    // with matching values
    //
      .get('.action-multiple-checkboxes [type="checkbox"]').check(['checkbox1', 'checkbox2']).should('be.checked')


    // **** Check Options ****
    //
    // cy.check() accepts options that control checking
    //
    // Ignore error checking prior to checking
    // like whether the element is visible, clickable or disabled
    // this checkbox below is disabled.
      .get('.action-checkboxes [disabled]')
        .check({force: true}).should('be.checked')

      .get('.action-radios [type="radio"]').check('radio3', {force: true}).should('be.checked')

  })


  it('cy.uncheck() - uncheck a checkbox element', function(){

    // By default, cy.uncheck() will uncheck all matching
    // checkbox elements in succession, one after another

    // https://on.cypress.io/api/uncheck
    cy
      .get('.action-check [type="checkbox"]')
        .not('[disabled]')
          .uncheck().should('not.be.checked')

    // **** Check Value ****
    //
    // cy.uncheck() accepts a value argument
    // that unchecks only checkboxes
    // with matching values
    //
      .get('.action-check [type="checkbox"]')
        .check('checkbox1')
        .uncheck('checkbox1').should('not.be.checked')

    // **** Uncheck Values ****
    //
    // cy.uncheck() accepts an array of values
    // that unchecks only checkboxes or radios
    // with matching values
    //
      .get('.action-check [type="checkbox"]')
        .check(['checkbox1', 'checkbox3'])
        .uncheck(['checkbox1', 'checkbox3']).should('not.be.checked')

    // **** Uncheck Options ****
    //
    // cy.uncheck() accepts options that control unchecking
    //
    // Ignore error checking prior to unchecking
    // like whether the element is visible, clickable or disabled
    // this checkbox below is disabled.
      .get('.action-check [disabled]')
        .uncheck({force: true}).should('not.be.checked')

  })

  it('cy.select() - select an option in a <select> element', function(){

    // https://on.cypress.io/api/select

    // Select the option with matching text content
    cy.get('.action-select').select('apples')

    // Select the option with matching value
    cy.get('.action-select').select('fr-bananas')

    // Select the options with matching text content
    cy.get('.action-select-multiple').select(['apples', 'oranges', 'bananas'])

    // Select the options with matching values
    cy.get('.action-select-multiple').select(['fr-apples', 'fr-oranges', 'fr-bananas'])

  })


})
