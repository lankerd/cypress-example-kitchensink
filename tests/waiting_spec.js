context('Waiting', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/waiting')
  })
  // **** Waiting ****
  //
  // Wait for a specific amount of ms before
  // continuing to the next command
  //
  // BE CAREFUL of adding unnecessary wait times:
  // https://on.cypress.io/guides/anti-patterns#section-adding-unnecessary-waits
  //
  // https://on.cypress.io/api/wait
  it('cy.wait() - wait for a specific amount of time', function(){

    cy
      .get(".wait-input1").type('Wait 1000ms after typing')
      .wait(1000)
      .get(".wait-input2").type('Wait 1000ms after typing')
      .wait(1000)
      .get(".wait-input3").type('Wait 1000ms after typing')
      .wait(1000)

  })

  //
  // Waiting for a specific resource to resolve
  // is covered within the cy.route() test below
  //

})
