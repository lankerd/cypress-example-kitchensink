context('Querying', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/querying')
  })

  // **** Querying DOM Elements ****
  //
  // Let's query for some DOM elements and make assertions
  // The most commonly used query is 'cy.get()', you can
  // think of this like the '$' in jQueury

  it('cy.get() - query DOM elements', function(){

    // https://on.cypress.io/api/get
    // We can get DOM elements by id
    cy.get('#query-btn').should('contain', 'Button')

    // We can get DOM elements by class
    cy.get('.query-btn').should('contain', 'Button')


    cy.get('#querying .well>button:first').should('contain', 'Button')
    //              ↲
    // we can CSS selectors just like jQuery

  })

  it('cy.contains() - query DOM elements with matching content', function(){

    // https://on.cypress.io/api/contains
    cy
      .get('.query-list')
        .contains('bananas').should('have.class', 'third')

      // we can even pass a regexp to `cy.contains()`
      .get('.query-list')
        .contains(/^b\w+/).should('have.class', 'third')

      // `cy.contains()` will return the first matched element
      .get('.query-list')
        .contains('apples').should('have.class', 'first')

      // passing a selector to contains will return the parent
      // selector containing the text
      .get('#querying')
        .contains('ul', 'oranges').should('have.class', 'query-list')

      // `cy.contains()` will favor input[type='submit'],
      // button, a, and label over deeper elements inside them
      // this will not return the <span> inside the button,
      // but the <button> itself
      .get('.query-button')
        .contains('Save Form').should('have.class', 'btn')

  })

  it('cy.within() - query DOM elements within a specific element', function(){

    // https://on.cypress.io/api/within
    cy.get('.query-form').within(function(){
      cy
        .get('input:first').should('have.attr', 'placeholder', 'Email')
        .get('input:last').should('have.attr', 'placeholder', 'Password')

    })

  })

  it('cy.root() - query the root DOM element', function(){

    // https://on.cypress.io/api/root
    // By default, root is the document
    cy.root().should('match', 'html')

    cy.get('.query-ul').within(function(){
      // In this within, the root is now the ul DOM element
      cy.root().should('have.class', 'query-ul')
    })

  })

})