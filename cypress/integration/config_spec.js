context('Cypress.config()', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/cypress-api/config')
  })

  // **** Config ****
  //

  it('Cypress.config() - get and set configuration options', function(){

    // https://on.cypress.io/api/config
    var myConfig = Cypress.config()

    expect(myConfig).to.have.property('animationDistanceThreshold', 5)
    expect(myConfig).to.have.property('baseUrl', null)
    expect(myConfig).to.have.property('commandTimeout', 4000)
    expect(myConfig).to.have.property('requestTimeout', 5000)
    expect(myConfig).to.have.property('responseTimeout', 20000)
    expect(myConfig).to.have.property('viewportHeight', 660)
    expect(myConfig).to.have.property('viewportWidth', 1000)
    expect(myConfig).to.have.property('pageLoadTimeout', 30000)
    expect(myConfig).to.have.property('waitForAnimations', true)


    // *** get a single configuration option **
    expect(Cypress.config('pageLoadTimeout')).to.eq(30000)


    // *** set a single configuration option **
    //
    // this will change the config for the rest of your tests!
    //
    Cypress.config('pageLoadTimeout', 20000)

    expect(Cypress.config('pageLoadTimeout')).to.eq(20000)

    Cypress.config('pageLoadTimeout', 30000)

  })

})