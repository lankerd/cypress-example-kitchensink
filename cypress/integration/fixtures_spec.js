context('Fixtures', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/fixtures')
  })
  // **** Fixtures ****
  //
  // Instead of writing a response inline you can
  // connect a response with a fixture file
  // located in _fixtures folder.

  it('cy.fixture() - load a fixture', function(){

    cy.server()

    // https://on.cypress.io/api/fixture
    cy
      .fixture('example.json').as('comment')

      .route(/comments/, '@comment').as('getComment')

      // we have code that gets a comment when
      // the button is clicked in scripts.js
      .get('.fixture-btn').click()

      .wait('@getComment').its('responseBody')
        .should('have.property', 'name')
          .and('include', 'Using fixtures to represent data')

    // you can also just write the fixture in the route
    cy
      .route(/comments/, 'fixture:example.json').as('getComment')

      // we have code that gets a comment when
      // the button is clicked in scripts.js
      .get('.fixture-btn').click()

      .wait('@getComment').its('responseBody')
        .should('have.property', 'name')
          .and('include', 'Using fixtures to represent data')

    // or write fx to represent fixture
    // by default it assumes it's .json
    cy
      .route(/comments/, 'fx:example').as('getComment')

      // we have code that gets a comment when
      // the button is clicked in scripts.js
      .get('.fixture-btn').click()

      .wait('@getComment').its('responseBody')
        .should('have.property', 'name')
          .and('include', 'Using fixtures to represent data')

  })

})