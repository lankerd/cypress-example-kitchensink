context('Viewport', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/viewport')
  })

  // **** Viewport ****
  //
  // Let's make some assertions based on
  // the size of our screen. This command
  // is great for checking responsive logic

  it('cy.viewport() - set the viewport size and dimension', function(){

    cy
      .get('#navbar').should('be.visible')

    // https://on.cypress.io/api/viewport
    cy.viewport(320, 480)

    // the navbar should have collapse since our screen is smaller
    cy
      .get('#navbar').should('not.be.visible')
      .get('.navbar-toggle').should('be.visible').click()
      .get('.nav').find('a').should('be.visible')

    // lets see what our app looks like on a super large screen
    cy.viewport(2999, 2999)

    // **** Viewport Presets ****
    //
    // cy.viewport() accepts a set of preset sizes
    // to easily set the screen to a device's width and height

    // We added a cy.wait() between each viewport change so you can see
    // the change otherwise it's a little too fast to see :)
    //
    cy
      .viewport('macbook-15')
      .wait(200)
      .viewport('macbook-13')
      .wait(200)
      .viewport('macbook-11')
      .wait(200)
      .viewport('ipad-2')
      .wait(200)
      .viewport('ipad-mini')
      .wait(200)
      .viewport('iphone-6+')
      .wait(200)
      .viewport('iphone-6')
      .wait(200)
      .viewport('iphone-5')
      .wait(200)
      .viewport('iphone-4')
      .wait(200)
      .viewport('iphone-3')
      .wait(200)

    // **** Viewport Orientation ****
    //
    // cy.viewport() accepts an orientation for all presets
    // the default orientation is 'portrait'
    //
    cy
      .viewport('ipad-2', 'portrait')
      .wait(200)
      .viewport('iphone-4', 'landscape')
      .wait(200)

    // The viewport will be reset back to the default dimensions
    // in between tests (the  default is set in cypress.json)

  })

})