context('Local Storage', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/local-storage')
  })
  // **** Local Storage ****
  //
  // Although local storage is automatically cleared
  // to maintain a clean state in between tests
  // sometimes we need to clear the local storage manually

  it('cy.clearLocalStorage() - clear all data in local storage', function(){

    // **** Clear all data in Local Storage ****
    //
    // https://on.cypress.io/api/clearlocalstorage
    cy
      .get(".ls-btn").click().then(function(){
        expect(localStorage.getItem('prop1')).to.eq('red')
        expect(localStorage.getItem('prop2')).to.eq('blue')
        expect(localStorage.getItem('prop3')).to.eq('magenta')
      })

      // clearLocalStorage() returns the localStorage object
      .clearLocalStorage().then(function(ls){
        expect(ls.getItem('prop1')).to.be.null
        expect(ls.getItem('prop2')).to.be.null
        expect(ls.getItem('prop3')).to.be.null
      })

    // **** Clear key matching string in Local Storage ****
    //
    cy
      .get(".ls-btn").click().then(function(){
        expect(localStorage.getItem('prop1')).to.eq('red')
        expect(localStorage.getItem('prop2')).to.eq('blue')
        expect(localStorage.getItem('prop3')).to.eq('magenta')
      })

      .clearLocalStorage('prop1').then(function(ls){
        expect(ls.getItem('prop1')).to.be.null
        expect(ls.getItem('prop2')).to.eq('blue')
        expect(ls.getItem('prop3')).to.eq('magenta')
      })

    // **** Clear key's matching regex in Local Storage ****
    //
    cy
      .get(".ls-btn").click().then(function(){
        expect(localStorage.getItem('prop1')).to.eq('red')
        expect(localStorage.getItem('prop2')).to.eq('blue')
        expect(localStorage.getItem('prop3')).to.eq('magenta')
      })

      .clearLocalStorage(/prop1|2/).then(function(ls){
        expect(ls.getItem('prop1')).to.be.null
        expect(ls.getItem('prop2')).to.be.null
        expect(ls.getItem('prop3')).to.eq('magenta')
      })

  })

})