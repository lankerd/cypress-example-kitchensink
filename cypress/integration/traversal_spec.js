context('Traversal', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/traversal')
  })

  // **** Traversing DOM Elements ****
  //
  // Let's query for some DOM elements and make assertions
  // The most commonly used query is 'cy.get()', you can
  // think of this like the '$' in jQueury

  it('cy.children() - get child DOM elements', function(){

    // https://on.cypress.io/api/children
    cy.get('.traversal-breadcrumb').children('.active').should('contain', 'Data')

  })

  it('cy.closest() - get closest ancestor DOM element', function(){

    // https://on.cypress.io/api/closest
    cy.get('.traversal-badge').closest('ul').should('have.class', 'list-group')

  })

  it('cy.eq() - get a DOM element at a specific index', function(){

    // https://on.cypress.io/api/eq
    cy.get('.traversal-list>li').eq(1).should('contain', 'siamese')

  })

  it('cy.filter() - get DOM elements that match the selector', function(){

    // https://on.cypress.io/api/filter
    cy.get('.traversal-nav>li').filter('.active').should('contain', 'About')

  })

  it('cy.find() - get descendant DOM elements of the selector', function(){

    // https://on.cypress.io/api/find
    cy.get('.traversal-pagination').find('li').find('a').should('have.length', 7)
  })

  it('cy.first() - get first DOM element', function(){

    // https://on.cypress.io/api/first
    cy.get('.traversal-table td').first().should('contain', '1')

  })

  it('cy.last() - get last DOM element', function(){

    // https://on.cypress.io/api/last
    cy.get('.traversal-buttons .btn').last().should('contain', 'Submit')

  })

  it('cy.next() - get next sibling DOM element', function(){

    // https://on.cypress.io/api/next
    cy.get('.traversal-ul').contains('apples').next().should('contain', 'oranges')

  })

  it('cy.not() - remove DOM elements from set of DOM elements', function(){

    // https://on.cypress.io/api/not
    cy.get('.traversal-disabled .btn').not('[disabled]').should('not.contain', 'Disabled')

  })

  it('cy.parents() - get parents DOM element from set of DOM elements', function(){

    // https://on.cypress.io/api/parents
    cy.get('.traversal-cite').parents().should('match', 'blockquote')

  })

  it('cy.siblings() - get all sibling DOM elements from set of DOM elements', function(){

    // https://on.cypress.io/api/siblings
    cy.get('.traversal-pills .active').siblings().should('have.length', 2)

  })


})